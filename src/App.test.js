import { render, screen } from "@testing-library/react";
import App from "./App";
import { Header } from "./Components/Header";
import { shallow, mount } from "enzyme";
import { RoadMap } from "./Components/roadmapPage/RoadMap";
import { Fieldset } from "./Components/roadmapPage/Fieldset";
import { Review } from "./Components/roadmapPage/Review";
import { SurveyPartOne } from "./Components/roadmapPage/SurveyPartOne";
import { SurveyPartTwo } from "./Components/roadmapPage/SurveyPartTwo";
import React from "react";

describe("App testing", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount(<RoadMap />);
  });
  // test("should update state of internet key by click event", () => {
  //   const changeValue = jest.fn();
  //   const wrapper = mount(<SurveyPartOne onClick={changeValue} />);
  //   const handleClick = jest.spyOn(React, "useState");
  //   handleClick.mockImplementation((value) => [value, changeValue]);
  //   wrapper.find("#internet1").simulate("click");
  //   expect(changeValue).toBeTruthy();
  // });
  test("component Review should receive initial value of internet", () => {
    // const wrapper = mount(<RoadMap />);
    wrapper.find("button").find("#next").simulate("click");
    wrapper.find("button").find("#next").simulate("click");
    wrapper.find("button").find("#next").simulate("click");
    expect(wrapper.find("#internet").text()).toBe("0");
  });
  test("component Review should receive updated value of internet from part1", () => {
    wrapper.find("#internet3").simulate("change");
    wrapper.find("button").find("#next").simulate("click");
    wrapper.find("button").find("#next").simulate("click");
    wrapper.find("button").find("#next").simulate("click");
    // console.log(wrapper.debug());
    expect(wrapper.find("#internet").text()).toBe("3");
  });
  test("component Review should receive updated value of BEM from part2", () => {
    wrapper.find("button").find("#next").simulate("click");
    wrapper.find("#bem3").simulate("change");
    wrapper.find("button").find("#next").simulate("click");
    wrapper.find("button").find("#next").simulate("click");
    // console.log(wrapper.debug());
    expect(wrapper.find("#bem").text()).toBe("3");
  });
  test("component Review should receive updated value of SSG from part3", () => {
    wrapper.find("button").find("#next").simulate("click");
    wrapper.find("button").find("#next").simulate("click");
    wrapper.find("#ssg1").simulate("change");
    wrapper.find("button").find("#next").simulate("click");
    // console.log(wrapper.debug());
    expect(wrapper.find("#ssg").text()).toBe("1");
  });
  test("render total result after state change, check `1балл`", () => {
    wrapper.find("#internet1").simulate("change");
    wrapper.find("button").find("#next").simulate("click");
    wrapper.find("button").find("#next").simulate("click");
    wrapper.find("button").find("#next").simulate("click");
    // console.log(wrapper.debug());
    expect(wrapper.find("#result").text()).toBe("1 балл");
  });
  test("render total result after state change, check `4балла`", () => {
    wrapper.find("#internet1").simulate("change");
    wrapper.find("#http3").simulate("change");
    wrapper.find("button").find("#next").simulate("click");
    wrapper.find("button").find("#next").simulate("click");
    wrapper.find("button").find("#next").simulate("click");
    // console.log(wrapper.debug());
    expect(wrapper.find("#result").text()).toBe("4 балла");
  });
  test("render values to state from parts 2 and 3, to see in Review, check `10баллов`", () => {
    wrapper.find("button").find("#next").simulate("click");
    wrapper.find("#git5").simulate("change");
    wrapper.find("button").find("#next").simulate("click");
    wrapper.find("#tests5").simulate("change");
    wrapper.find("button").find("#next").simulate("click");
    expect(wrapper.find("#result").text()).toBe("10 баллов");
  });
  test("render array length that arrives to Review component", () => {
    wrapper.find("button").find("#next").simulate("click");
    wrapper.find("button").find("#next").simulate("click");
    wrapper.find("button").find("#next").simulate("click");
    expect(wrapper.find("button").length).toBe(26);
  });
});
