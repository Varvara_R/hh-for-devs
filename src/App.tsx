import { useCallback } from 'react'
import './App.css'
import { useSelector, useDispatch } from 'react-redux'
import { Routes, Route } from 'react-router-dom'
import Home from './Components/common/Home'
import Header from './Components/common/Header'
import Graphes from './Components/hhData/Graphes'
import Registration from './Components/common/Registration'
import Auth from './Components/common/Auth'
import Skills from './Components/skills/Skills'
import Roadmaps from './Components/roadmaps/Roadmaps'
import { AppStateType } from './redux/reducers/rootReducer'
import { userAction } from './redux/actions/userAction'
import Footer from './Components/common/Footer'
import RoadmapCard from './Components/roadmaps/RoadmapCard'

function App() {
  const isLoggedIn = useSelector((state: AppStateType) => state.user.isLoggedIn)
  const dispatch = useDispatch()
  const logout = useCallback(() => {
    dispatch(userAction.logout())
  }, [])
  return (
    <div className="App">
      <Header isLoggedIn={isLoggedIn} logout={logout} />
      <div className="app-content">
        <Routes>
          {!isLoggedIn && (
            <>
              <Route path="/login" element={<Auth />} />
              <Route path="/registration" element={<Registration />} />
            </>
          )}
          <Route path="/" element={<Home />} />

          <Route path="/home" element={<Home />} />
          <Route path="/graph" element={<Graphes />} />
          <Route path="/skills" element={<Skills />} />
          <Route path="/roadmaps" element={<Roadmaps />} />
          <Route path="/roadmaps/:roadmapId" element={<RoadmapCard />} />

          <Route path="*" element={<Home />} />
        </Routes>
      </div>
      <Footer />
    </div>
  )
}

export default App
