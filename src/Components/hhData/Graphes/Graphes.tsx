import React, { useCallback, useEffect, useState } from 'react'
import Graph from '../Graph'
import Typography from '@material-ui/core/Typography'
import { vacanciesAction } from '../../../redux/actions/vacanciesAction'
import { AppStateType } from '../../../redux/reducers/rootReducer'
import { useDispatch, useSelector } from 'react-redux'
import { IGraphData } from '../../../interfaces/graph'
import './Graphes.scss'
import CircularProgress from '@mui/material/CircularProgress'

function getSalariesFrequencies(salaries: number[]) {
  return salaries.reduce((tally, amount) => {
    tally[amount] = (tally[amount] || 0) + 1
    return tally
  }, {})
}

const Graphes = () => {
  let objForSkillsGraph = {
    javaScript: 0,
    html: 0,
    css: 0,
    react: 0,
    vue: 0,
    angular: 0,
    redux: 0,
    typeScript: 0,
    buildTools: 0,
    figma: 0,
    scss: 0,
  }
  const vacancies = useSelector(
    (state: AppStateType) => state.vacancies.fetchVacancies
  )
  const dispatch = useDispatch()
  const getVacancies = useCallback(() => {
    dispatch(vacanciesAction.getVacancies())
  }, [])

  const [salaries, setSalaries] = useState<IGraphData[] | null>(null)
  const [averageSalary, setAverageSalary] = useState<number | null>(null)
  const [skills, setSkills] = useState<IGraphData[] | null>(null)

  const getSalaries = () => {
    if (vacancies && vacancies.length) {
      let salariesResult: number[] = []
      let notDefinedSalary: number = 0
      vacancies.map((vacancy) => {
        if (vacancy.salary && vacancy.salary.currency === 'RUR') {
          return vacancy.salary.from
            ? salariesResult.push(vacancy.salary.from)
            : vacancy.salary.to
            ? salariesResult.push(vacancy.salary.to)
            : notDefinedSalary++
        }
      })

      if (salariesResult.length) {
        const frequentSalaries: any = getSalariesFrequencies(salariesResult)
        let salariesData: IGraphData[] = []
        for (const [key, value] of Object.entries(frequentSalaries)) {
          const newSalaryData: IGraphData = {
            name: key,
            value: value,
          }
          salariesData.push(newSalaryData)
        }
        setSalaries(salariesData)
        const average: number = salariesResult.reduce(
          (total, amount, index, array) => {
            total += amount
            if (index === array.length - 1) {
              return total / array.length
            } else {
              return total
            }
          }
        )
        setAverageSalary(average)
      }
    } else {
      setSalaries(null)
      setAverageSalary(null)
    }
  }

  const getSkills = () => {
    if (vacancies && vacancies.length) {
      let skillsResult: string[] = []
      vacancies.map((vacancy) => {
        if (vacancy.snippet && vacancy.snippet.requirement) {
          skillsResult.push(vacancy.snippet.requirement)
        }
      })
      if (skillsResult.length) {
        const splittedSkills = skillsResult.map((item) =>
          item.toLowerCase().split(' ')
        )
        splittedSkills.map((elem) => findMatches(elem))
        let skillsData: IGraphData[] = []
        for (const [key, value] of Object.entries(objForSkillsGraph)) {
          const newData: IGraphData = {
            name: key,
            value: value,
          }
          skillsData.push(newData)
        }
        const sortedSkillsData = sortByBiggestValue(skillsData)
        console.log('sortedSkillsData', sortedSkillsData)
        setSkills(sortedSkillsData)
      }
    }
  }

  useEffect(() => {
    getVacancies()
  }, [])

  useEffect(() => {
    getSalaries()
    getSkills()
  }, [vacancies])

  function findMatches(skillsArrayFromHh) {
    skillsArrayFromHh.map((item) => {
      if (
        item.includes(
          'javascript' || 'js' || 'es6' || 'es6+' || 'es5' || 'es5+'
        )
      ) {
        objForSkillsGraph.javaScript++
      }
      if (item.includes('html' || 'html5')) {
        objForSkillsGraph.html++
      }
      if (item.includes('css' || 'css3')) {
        objForSkillsGraph.css++
      }
      if (item.includes('react')) {
        objForSkillsGraph.react++
      }
      if (item.includes('vue')) {
        objForSkillsGraph.vue++
      }
      if (item.includes('angular')) {
        objForSkillsGraph.angular++
      }
      if (item.includes('redux')) {
        objForSkillsGraph.redux++
      }
      if (item.includes('typescript' || 'ts')) {
        objForSkillsGraph.typeScript++
      }
      if (item.includes('build')) {
        objForSkillsGraph.buildTools++
      }
      if (item.includes('figma' || 'ps' || 'photoshop')) {
        objForSkillsGraph.figma++
      }
      if (item.includes('scss' || 'ps' || 'photoshop')) {
        objForSkillsGraph.scss++
      }
    })
  }

  function sortByBiggestValue(skillsData: IGraphData[]) {
    let updateSkillsData: any = []
    skillsData.map((skill) => {
      if (!skill.value || skill.value === 0) {
        return
      } else {
        updateSkillsData.push(skill)
      }
    })
    return updateSkillsData.sort((a, b) => (a.value < b.value ? 1 : -1))
  }

  if ((!salaries && !averageSalary) || !skills) {
    return (
      <div className="loading">
        <CircularProgress size={70} />
      </div>
    )
  }

  return (
    <div className="graphes">
      <Typography variant="h6" align="center" color="textSecondary" paragraph>
        Посмотри, какие скиллы самые востребованные и какие зарплаты предлагают
        за эти скиллы в СПб
      </Typography>
      {salaries && averageSalary ? (
        <>
          <Typography variant="h6">
            Средняя зарплата юного разработчика JS на текущий момент:{' '}
            {averageSalary.toLocaleString()} руб.
          </Typography>
          <Graph data={salaries} bar={14} />
        </>
      ) : (
        <p>
          Technical problems, impossible to get data from HH.ru right now.
          Please try later.
        </p>
      )}
      {skills ? (
        <>
          <Typography variant="h6">
            Самые востребованные скиллы юного разработчика:
          </Typography>
          <Graph data={skills} bar={20} />
        </>
      ) : (
        <p>
          Technical problems, impossible to get data from HH.ru right now.
          Please try later.
        </p>
      )}
    </div>
  )
}

export default Graphes
