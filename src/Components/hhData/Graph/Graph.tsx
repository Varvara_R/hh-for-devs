import React, { FC } from "react";
import { IGraphData } from "../../../interfaces/graph";
import {
  ComposedChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from "recharts";

type Props = {
  data: Array<IGraphData>,
  bar: number
}
const Graph: FC<Props> = ({ data, bar }) => {

    return (
      <ResponsiveContainer
        className="containerGraph"
        width="100%"
        height="100%"
      >
        <ComposedChart
          layout="vertical"
          width={500}
          height={600}
          data={data}
          margin={{
            top: 20,
            right: 20,
            bottom: 20,
            left: 20,
          }}
        >
          <CartesianGrid stroke="#f5f5f5" />
          <XAxis type="number" />
          <YAxis dataKey="name" type="category" scale="band" />
          <Tooltip />
          <Legend />
          {/*<Area dataKey="amt" fill="#8884d8" stroke="#8884d8" />*/}
          <Bar dataKey="value" barSize={bar} fill="#413ea0" />
          {/*<Line dataKey="uv" stroke="#ff7300" />*/}
        </ComposedChart>
      </ResponsiveContainer>
    );
  
};

export default Graph;
