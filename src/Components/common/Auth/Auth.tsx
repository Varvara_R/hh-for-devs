import React, { useState, useEffect, useCallback } from 'react'
import TextField from '@mui/material/TextField'
import Button from '@mui/material/Button'
import { useDispatch, useSelector } from 'react-redux'
import { userAction } from '../../../redux/actions/userAction'
import { Link } from 'react-router-dom'
import './Auth.scss'
import { AppStateType } from '../../../redux/reducers/rootReducer'
import LoadingButton from '@mui/lab/LoadingButton'

const Auth = () => {
  const currentUser = useSelector(
    (state: AppStateType) => state.user.currentUser
  )
  const isLogging = useSelector((state: AppStateType) => state.user.isLogging)
  const dispatch = useDispatch()
  const login = useCallback((email: string, password: string) => {
    dispatch(userAction.login(email, password))
  }, [])
  const [email, setEmail] = useState<string>('')
  const [password, setPassword] = useState<string>('')
  const [submitted, setSubmitted] = useState<boolean>(false)

  const onChangeEmail = (evt) => {
    const value = evt.target.value !== '' ? evt.target.value : ''
    if (value) {
      setEmail(value)
    }
  }

  const onChangePassword = (evt) => {
    const value = evt.target.value !== '' ? evt.target.value : ''
    if (value) {
      setPassword(value)
    }
  }

  const handleSubmit = (evt) => {
    evt.preventDefault()
    setSubmitted(true)
    if (email && password) {
      login(email, password)
    }
  }

  return (
    <div className="auth">
      <form className="auth__form" onSubmit={handleSubmit}>
        <div className="auth__title">
          <h3> Sign in</h3>
        </div>
        <div className="auth__item">
          <TextField
            error={submitted && !email}
            id="filled-error-helper-text"
            label="Username"
            value={email}
            helperText={submitted && !email ? 'Incorrect username' : ''}
            variant="filled"
            onChange={onChangeEmail}
          />
        </div>
        <div className="auth__item">
          <TextField
            error={submitted && !password}
            id="filled-error-helper-text"
            label="Password"
            value={password}
            helperText={submitted && !password ? 'Incorrect password' : ''}
            variant="filled"
            onChange={onChangePassword}
          />
        </div>
        <LoadingButton
          variant="contained"
          color="primary"
          loading={isLogging}
          onClick={handleSubmit}>
          Submit
        </LoadingButton>
        <div className="auth__noAccount">
          <p>
            If you don't have account yet, please{' '}
            <Link to="/registration">
              <span> sign up</span>
            </Link>
          </p>
        </div>
      </form>
    </div>
  )
}

export default Auth
