import React, { FC } from 'react'
import { Link } from 'react-router-dom'
import './Dropdown.scss'

type Props = {
  logout: () => void
}
const Dropdown: FC<Props> = ({ logout }) => {
  return (
    <div className="dropdown animated faster">
      <div className="dropdown__menu">
        <span className="dropdown__item">Profile</span>
        <hr />
        <span className="dropdown__item" onClick={logout}>
          Sign out
        </span>
      </div>
    </div>
  )
}

export default Dropdown
