import React from 'react'
import './Footer.scss'
import EmailIcon from '@mui/icons-material/Email'

const Footer = () => {
  return (
    <footer className="app-footer">
      <div className="app-footer__column">
        <h5 className="footer-title">Developed by Varvara Rubtsova:</h5>
        <p className="footer-text">
          <span>Backend:</span> Node.js, Express, MongoDB
        </p>
        <p className="footer-text">
          <span>Frontend:</span> JS, TypeScript, React/Redux, SASS
        </p>
      </div>
      <div className="app-footer__column">
        <h5 className="footer-title">Contact to developer:</h5>
        <a href="mailto:varya.rubtsova@gmail.com" className="email-container">
          <EmailIcon className="material-icons" />
          <span className="email">varya.rubtsova@gmail.com</span>
        </a>
      </div>
    </footer>
  )
}

export default Footer
