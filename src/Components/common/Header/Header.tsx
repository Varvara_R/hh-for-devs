import React, { FC, useState, useEffect } from 'react'
import CssBaseline from '@material-ui/core/CssBaseline'
import AppBar from '@material-ui/core/AppBar'
// import Toolbar from "@material-ui/core/Toolbar";
import Typography from '@material-ui/core/Typography'
import ClickAwayListener from '@mui/material/ClickAwayListener'
// import Box from '@mui/material/Box';
import Navbar from '../NavBar'
import { makeCancelable } from '../../../helperFns'
import Dropdown from '../Dropdown'
import { Link } from 'react-router-dom'
import './Header.scss'
import { useSelector } from 'react-redux'
import { AppStateType } from '../../../redux/reducers/rootReducer'

type Props = {
  isLoggedIn: boolean
  logout: () => void
}
const Header: FC<Props> = ({ isLoggedIn, logout }) => {
  const currentUser = useSelector(
    (state: AppStateType) => state.user.currentUser
  )
  const [image, setImage] = useState()
  const [isDropDownVisible, setDropDownVisible] = useState<boolean>(false)

  useEffect(() => {
    window.addEventListener('scroll', changeBgcOnScroll, false)
    // const cancelablePromise = makeCancelable(import(`../img/${ user.id }.jpg`));
    const cancelablePromise = makeCancelable(
      import(`../../../img/default_avatar.jpg`)
    )
    cancelablePromise.promise
      .then((image) => {
        setImage(image.default)
      })
      .catch((reason) => {
        console.log('Cancel image update due to:', reason.isCanceled)
      })

    return () => {
      window.removeEventListener('scroll', changeBgcOnScroll, false)
      cancelablePromise.cancel()
    }
  }, [image])

  function changeBgcOnScroll() {
    const scrolled = window.pageYOffset || document.documentElement.scrollTop
    let elem: any = document.querySelector('.dropdown')
    if (elem) {
      elem.style.backgroundColor =
        scrolled == 0 ? 'hsl(211, 30%, 20%)' : 'hsla(211, 30%, 20%, .9)'
    }
  }

  const changeDropDownVisibility = () => {
    setDropDownVisible(!isDropDownVisible)
  }

  function onClickOutsideHandler(e) {
    setDropDownVisible(false)
  }

  return (
    <>
      <CssBaseline />
      <AppBar position="sticky">
        {/* <Toolbar> */}
        <div className="header">
          {/*<CameraIcon className={classes.icon} />*/}
          <div className="left-side">
            <Typography variant="h6" color="inherit" noWrap>
              <Link className="header__link" to="/home">
                Junior Frontend.Begin
              </Link>
            </Typography>
            <Navbar />
          </div>

          {isLoggedIn ? (
            <ClickAwayListener onClickAway={onClickOutsideHandler}>
              <div className="profile profileToggle">
                <div
                  className="profile__link profileToggle"
                  onClick={changeDropDownVisibility}>
                  <p className="profile__username profileToggle">
                    {currentUser ? currentUser.name : 'Username'}
                  </p>
                  <img className="profile__image profileToggle" src={image} />
                </div>

                {isDropDownVisible ? <Dropdown logout={logout} /> : null}
              </div>
            </ClickAwayListener>
          ) : (
            <Link to="/login">
              <button className="autorizationBtn">Auth</button>
            </Link>
          )}
        </div>
        {/* </Toolbar> */}
      </AppBar>
    </>
  )
}

export default Header
