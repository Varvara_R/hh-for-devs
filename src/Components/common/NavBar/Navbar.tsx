import React from 'react'
import { NavLink } from 'react-router-dom'
import './Navbar.scss'

const Navbar = () => {
  return (
    <div className="nav-wrapper">
      <ul className="nav">
        <li className="nav__item">
          <NavLink to="/graph" className="nav__link">
            {' '}
            HH data
          </NavLink>
        </li>
        <li className="nav__item">
          <NavLink to="/skills" className="nav__link">
            Skills
          </NavLink>
        </li>
        <li className="nav__item">
          <NavLink to="/roadmaps" className="nav__link">
            Roadmaps
          </NavLink>
        </li>
      </ul>
    </div>
  )
}

export default Navbar
