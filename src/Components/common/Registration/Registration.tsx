import React, { useState, useEffect, useCallback } from 'react'
import TextField from '@mui/material/TextField'
import Button from '@mui/material/Button'
import { useDispatch } from 'react-redux'
import { userAction } from '../../../redux/actions/userAction'

const Registration = () => {
  const dispatch = useDispatch()
  const registration = useCallback(
    (email: string, password: string, name: string) => {
      dispatch(userAction.registration(email, password, name))
    },
    []
  )
  const [email, setEmail] = useState<string>('')
  const [password, setPassword] = useState<string>('')
  const [name, setName] = useState<string>('')
  const [submitted, setSubmitted] = useState<boolean>(false)

  const onChangeEmail = (evt) => {
    const value = evt.target.value !== '' ? evt.target.value : ''
    setEmail(value)
  }

  const onChangePassword = (evt) => {
    const value = evt.target.value !== '' ? evt.target.value : ''
    setPassword(value)
  }
  const onChangeName = (evt) => {
    const value = evt.target.value !== '' ? evt.target.value : ''
    setName(value)
  }

  const handleSubmit = (evt) => {
    setSubmitted(true)
    evt.preventDefault()
    if (email && password && name) {
      registration(email, password, name)
    }
  }

  return (
    <div className="auth">
      <form className="auth__form" onSubmit={handleSubmit}>
        <div className="auth__title">
          <h3> Sign up</h3>
        </div>
        <div className="auth__item">
          <TextField
            error={submitted && !email}
            id="filled-error-helper-text"
            label="Username"
            value={email}
            helperText={submitted && !email ? 'Incorrect username' : ''}
            variant="filled"
            onChange={onChangeEmail}
          />
        </div>
        <div className="auth__item">
          <TextField
            error={submitted && !password}
            id="filled-error-helper-text"
            label="Password"
            value={password}
            helperText={submitted && !password ? 'Incorrect password' : ''}
            variant="filled"
            onChange={onChangePassword}
          />
        </div>
        <div className="auth__item">
          <TextField
            error={submitted && !name}
            id="filled-error-helper-text"
            label="Name"
            value={name}
            helperText={submitted && !name ? 'Incorrect name' : ''}
            variant="filled"
            onChange={onChangeName}
          />
        </div>
        <Button variant="contained" color="primary" onClick={handleSubmit}>
          Submit
        </Button>
      </form>
    </div>
  )
}

export default Registration
