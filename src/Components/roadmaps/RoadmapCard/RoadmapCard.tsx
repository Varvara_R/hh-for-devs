import React, { useState, useCallback, useEffect, useRef } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import './RoadmapCard.scss'
import CircularProgress from '@mui/material/CircularProgress'
import { AppStateType } from '../../../redux/reducers/rootReducer'
import { useParams } from 'react-router-dom'
import { roadmapActions } from '../../../redux/actions/roadmapActions'
import ReactFlow, {
  ReactFlowProvider,
  Controls,
  Edge,
} from 'react-flow-renderer'
import LeftSideBar from './LeftSideBar'
import { Node } from 'react-flow-renderer'
import { IRoadmap } from '../../../interfaces/roadmap.interface'

const RoadmapCard = () => {
  const { roadmapId } = useParams()

  const roadmap = useSelector((state: AppStateType) => state.roadmaps.roadmap)
  const dispatch = useDispatch()
  const getRoadmapById = useCallback((id: string) => {
    dispatch(roadmapActions.getRoadmapById(id))
  }, [])
  const updateRoadmap = useCallback((roadmap: IRoadmap) => {
    dispatch(roadmapActions.update(roadmap))
  }, [])

  const [reactFlowInstance, setReactFlowInstance] = useState<any>(null)

  const reactFlowWrapper = useRef<HTMLDivElement | null>(null)

  const onConnect = (params) => {
    const newEdge: Edge = {
      id: `e${params.source}-${params.target}`,
      source: params.source,
      target: params.target,
    }
    if (roadmap) {
      roadmap.data.elements.push(newEdge)
      updateRoadmap(roadmap)
    }
  }
  const onElementsRemove = (elementsToRemove) => {
    if (roadmap) {
      elementsToRemove.forEach((element) => {
        const index: number = roadmap.data.elements.findIndex(
          (item) => item.id === element.id
        )
        if (index >= 0) {
          roadmap.data.elements.splice(index, 1)
        }
      })
      updateRoadmap(roadmap)
    }
  }

  const onLoad = (_reactFlowInstance) =>
    setReactFlowInstance(_reactFlowInstance)

  const onDragOver = (event) => {
    event.preventDefault()
    event.dataTransfer.dropEffect = 'move'
  }

  const onDrop = (event) => {
    event.preventDefault()

    if (
      roadmap &&
      reactFlowWrapper &&
      reactFlowWrapper.current &&
      reactFlowInstance
    ) {
      const reactFlowBounds = reactFlowWrapper.current.getBoundingClientRect()
      const type = event.dataTransfer.getData('application/reactflow')
      const position = reactFlowInstance.project({
        x: event.clientX - reactFlowBounds.left,
        y: event.clientY - reactFlowBounds.top,
      })
      const newId: string = `${+roadmap.data.currentId + 1}`
      const newNode: Node = {
        id: newId,
        type,
        position,
        data: { label: `${type} node` },
      }
      roadmap.data.elements.push(newNode)
      roadmap.data.currentId = newId
      updateRoadmap(roadmap)
    }
  }

  useEffect(() => {
    if (roadmapId) {
      getRoadmapById(roadmapId)
    }
  }, [])

  if (!roadmap) {
    return (
      <div className="loading">
        <CircularProgress size={70} />
      </div>
    )
  }
  return (
    <div className="rc">
      <div className="rc__title">
        <h2 className="name">{roadmap.title}'s editor</h2>
      </div>

      <div className="rc__editor">
        <ReactFlowProvider>
          <LeftSideBar />
          <div className="react_flow_wrapper" ref={reactFlowWrapper}>
            <ReactFlow
              elements={roadmap.data.elements}
              onConnect={onConnect}
              onElementsRemove={onElementsRemove}
              onLoad={onLoad}
              onDrop={onDrop}
              onDragOver={onDragOver}>
              <Controls />
            </ReactFlow>
          </div>
        </ReactFlowProvider>
      </div>
    </div>
  )
}

export default RoadmapCard
