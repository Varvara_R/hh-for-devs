import React, { FC } from 'react'
import './LeftSideBar.scss'

const LeftSideBar = () => {
  const onDragStart = (event, nodeType) => {
    event.dataTransfer.setData('application/reactflow', nodeType)
    event.dataTransfer.effectAllowed = 'move'
  }

  return (
    <aside className="leftSideBar">
      <div className="options">
        <p>hello</p>
      </div>
      <div className="tabs-area">
        <div
          className="dndnode input"
          onDragStart={(event) => onDragStart(event, 'input')}
          draggable>
          Input Node
        </div>
        <div
          className="dndnode default"
          onDragStart={(event) => onDragStart(event, 'default')}
          draggable>
          Default Node
        </div>
        <div
          className="dndnode output"
          onDragStart={(event) => onDragStart(event, 'output')}
          draggable>
          Output Node
        </div>
      </div>
    </aside>
  )
}

export default LeftSideBar
