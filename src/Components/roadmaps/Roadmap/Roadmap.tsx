import React, { useState, useEffect, FC } from 'react'
import { IRoadmap } from '../../../interfaces/roadmap.interface'
import SkillPhoto from '../../skills/SkillPhoto'
import './Roadmap.scss'
import { Link } from 'react-router-dom'

type Props = {
  roadmap: IRoadmap
}
const Roadmap: FC<Props> = ({ roadmap }) => {
  const [maxPhoto, setMaxPhoto] = useState<number>(4)

  const getSkillPhotosLine = () => {
    if (roadmap.data && roadmap.data.skills) {
      if (roadmap.data.skills.length === 0) {
        return
      } else if (roadmap.data.skills.length <= maxPhoto) {
        return roadmap.data.skills.map((skillName) => {
          return <SkillPhoto key={skillName} skillTitle={skillName} />
        })
      } else {
        const slicedSkills = roadmap.data.skills.slice(0, maxPhoto)
        return slicedSkills.map((skillName) => {
          return <SkillPhoto key={skillName} skillTitle={skillName} />
        })
      }
    }
  }
  return (
    <div className="roadmap">
      {roadmap._id ? (
        <Link to={`/roadmaps/${roadmap._id}`} className="roadmap__title">
          <h4>{roadmap.title}</h4>
        </Link>
      ) : (
        <div className="roadmap__title">
          <h4>{roadmap.title}</h4>
        </div>
      )}

      <div className="roadmap__content">
        <div className="info">
          <span className="creation">Created: {roadmap.created}</span>
          {roadmap.speciality && (
            <span className="speciality">{roadmap.speciality}</span>
          )}
        </div>
        {roadmap.description && (
          <p className="description">{roadmap.description}</p>
        )}
      </div>
      <div className="roadmap__footer">
        <div className="photos">{getSkillPhotosLine()}</div>
      </div>
    </div>
  )
}

export default Roadmap
