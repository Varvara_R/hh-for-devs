import React, { useCallback, useState, useEffect } from 'react'
import { AppStateType } from '../../../redux/reducers/rootReducer'
import './Roadmaps.scss'
import { useSelector, useDispatch } from 'react-redux'
import { roadmapActions } from '../../../redux/actions/roadmapActions'
import Autocomplete from '@mui/material/Autocomplete'
import TextField from '@mui/material/TextField'
import Button from '@mui/material/Button'
import { Add } from '@mui/icons-material'
import Roadmap from '../Roadmap'
import NewRoadmapForm from '../NewRoadmapForm'
import { Modal } from '@material-ui/core'
import { IRoadmap } from '../../../interfaces/roadmap.interface'
import CircularProgress from '@mui/material/CircularProgress'

const Roadmaps = () => {
  const [isNewRoadmapFormShown, setNewRoadmapFormShow] =
    useState<boolean>(false)

  const isLoggedIn = useSelector((state: AppStateType) => state.user.isLoggedIn)
  const roadmaps = useSelector((state: AppStateType) => state.roadmaps.roadmaps)
  const roadmapsCount = useSelector(
    (state: AppStateType) => state.roadmaps.count
  )
  const admin = useSelector((state: AppStateType) =>
    state.user.currentUser ? state.user.currentUser.admin : false
  )

  const dispatch = useDispatch()
  const getAllRoadmaps = useCallback(() => {
    dispatch(roadmapActions.getAllRoadmaps())
  }, [])
  const addNewRoadmap = useCallback((newRoadmap: IRoadmap) => {
    dispatch(roadmapActions.create(newRoadmap))
  }, [])

  const showNewRoadmapForm = () => {
    setNewRoadmapFormShow(true)
  }
  const hideNewRoadmapForm = () => {
    setNewRoadmapFormShow(false)
  }
  useEffect(() => {
    getAllRoadmaps()
  }, [])

  if (!roadmaps) {
    return (
      <div className="loading">
        <CircularProgress size={70} />
      </div>
    )
  }

  return (
    <>
      <div className="roadmaps">
        {roadmaps && (
          <div className="roadmaps__searchBar">
            <Autocomplete
              sx={
                isLoggedIn && admin
                  ? { width: '94%', marginRight: '10px' }
                  : { width: '100%' }
              }
              options={roadmaps}
              getOptionLabel={(option) => option.title}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="Search roadmap by name"
                  placeholder="Search for roadmap..."
                />
              )}
            />

            {isLoggedIn && admin && (
              <Button onClick={showNewRoadmapForm}>
                <div className="material-icons">
                  <Add />
                </div>
              </Button>
            )}
          </div>
        )}

        <div className="roadmaps__container">
          {roadmaps && roadmaps.length > 0 ? (
            roadmaps.map((roadmap) => {
              return <Roadmap key={roadmap.title} roadmap={roadmap} />
            })
          ) : (
            <p> No roadmaps were found</p>
          )}
        </div>
      </div>

      {isLoggedIn && admin && (
        <Modal
          open={isNewRoadmapFormShown}
          onClose={hideNewRoadmapForm}
          aria-labelledby="modal-new-roadmap"
          aria-describedby="modal">
          <NewRoadmapForm
            addNewRoadmap={addNewRoadmap}
            hideModal={hideNewRoadmapForm}
          />
        </Modal>
      )}
    </>
  )
}

export default Roadmaps
