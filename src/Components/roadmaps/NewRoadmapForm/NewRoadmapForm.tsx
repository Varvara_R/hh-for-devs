import React, { useState, useEffect, FC } from 'react'
import './NewRoadmapForm.scss'
import TextField from '@mui/material/TextField'
import TextareaAutosize from '@mui/material/TextareaAutosize'
import Button from '@mui/material/Button'
import { IRoadmap } from '../../../interfaces/roadmap.interface'

type Props = {
  addNewRoadmap: (newRoadmap: IRoadmap) => void
  hideModal: () => void
}
const NewRoadmapForm: FC<Props> = ({ addNewRoadmap, hideModal }) => {
  const [title, setTitle] = useState<string>('')
  const [speciality, setSpeciality] = useState<string>('')
  const [description, setDescription] = useState<string>('')

  const createRoadmap = () => {
    let today = new Date()
    if (title && speciality) {
      const newRoadmap: any = {
        title,
        data: {
          elements: [],
          currentId: '0',
          skills: [],
        },
        created: `${today.getFullYear}-${
          today.getMonth() + 1
        }-${today.getDate()}`,
        description,
        speciality,
      }
      addNewRoadmap(newRoadmap)
    }
    hideModal()
  }

  const onChangeTitle = (e) => {
    const value = e.target.value ? e.target.value : ''
    if (value) {
      setTitle(value)
    }
  }
  const onChangeSpeciality = (e) => {
    const value = e.target.value ? e.target.value : ''
    if (value) {
      setSpeciality(value)
    }
  }
  const onChangeDescription = (e) => {
    const value = e.target.value ? e.target.value : ''
    if (value) {
      setDescription(value)
    }
  }

  return (
    <div className="newRoadmap">
      <h4 className="newRoadmap__title">Create new roadmap</h4>
      <div className="newRoadmap__form">
        <TextField
          label="New roadmap title"
          //   value={title}
          variant="filled"
          onChange={onChangeTitle}
          className="form-textfield"
        />
        <TextField
          label="New roadmap speciality"
          //   value={speciality}
          variant="filled"
          onChange={onChangeSpeciality}
          className="form-textfield"
        />
        <TextareaAutosize
          minRows={3}
          placeholder="New roadmap description"
          onChange={onChangeDescription}
          className="form-textfield"
        />
      </div>
      <div className="newRoadmap__btns">
        <Button variant="outlined" onClick={hideModal}>
          Cancel
        </Button>
        <Button
          variant="contained"
          onClick={createRoadmap}
          disabled={!title || !speciality}>
          Submit
        </Button>
      </div>
    </div>
  )
}

export default NewRoadmapForm
