import React, { FC, useState, useEffect } from 'react'
import { ISkill } from '../../../interfaces/skill.interface'
import TextField from '@mui/material/TextField'
import { styled } from '@mui/material/styles'
import Button from '@mui/material/Button'
import './NewSkillForm.scss'

const Input = styled('input')({
  display: 'none',
})

type Props = {
  hideModal: () => void
  addNewSkill: (newSkill: ISkill) => void
}
const NewSkillForm: FC<Props> = ({ hideModal, addNewSkill }) => {
  const [newSkill, setNewSkill] = useState<ISkill>({
    title: '',
    type: '',
    subType: '',
  })

  const onChangeTitle = (e) => {
    const value = e.target.value === '' ? null : e.target.value
    setNewSkill((newSkill) => ({
      ...newSkill,
      title: value,
    }))
  }
  const onChangeType = (e) => {
    const value = e.target.value === '' ? null : e.target.value
    setNewSkill((newSkill) => ({
      ...newSkill,
      type: value,
    }))
  }
  const onChangeSubType = (e) => {
    const value = e.target.value === '' ? null : e.target.value
    setNewSkill((newSkill) => ({
      ...newSkill,
      subType: value,
    }))
  }
  const onChangeOfficialSite = (e) => {
    const value = e.target.value === '' ? null : e.target.value
    setNewSkill((newSkill) => ({
      ...newSkill,
      officialSite: value,
    }))
  }
  const onChangeWiki = (e) => {
    const value = e.target.value === '' ? null : e.target.value
    setNewSkill((newSkill) => ({
      ...newSkill,
      wiki: value,
    }))
  }
  const onChangeGit = (e) => {
    const value = e.target.value === '' ? null : e.target.value
    setNewSkill((newSkill) => ({
      ...newSkill,
      git: value,
    }))
  }
  const onChangeLogo = (e) => {
    const file = e.target.files[0]
    setNewSkill((newSkill) => ({
      ...newSkill,
      logo: URL.createObjectURL(file),
    }))
  }
  const onSave = () => {
    if (newSkill) {
      addNewSkill(newSkill)
    }
    hideModal()
  }
  return (
    <div className="newSkill">
      <div className="newSkill__title">
        <h4>Add new skill</h4>
      </div>
      <div className="newSkill__form">
        <TextField
          label="Skill title"
          // value={ newSkill.title }
          variant="filled"
          onChange={onChangeTitle}
          className="form-textfield"
        />
        <TextField
          label="Skill type"
          // value={ newSkill.type }
          variant="filled"
          onChange={onChangeType}
          className="form-textfield"
        />
        <TextField
          label="Skill subType"
          // value={ newSkill.subType }
          variant="filled"
          onChange={onChangeSubType}
          className="form-textfield"
        />
        <TextField
          label="Skill official site"
          // value={ newSkill.officialSite }
          variant="filled"
          onChange={onChangeOfficialSite}
          className="form-textfield"
        />
        <TextField
          label="Skill wiki"
          // value={ newSkill.wiki }
          variant="filled"
          onChange={onChangeWiki}
          className="form-textfield"
        />
        <TextField
          label="Skill git"
          // value={ newSkill.git }
          variant="filled"
          onChange={onChangeGit}
          className="form-textfield"
        />

        <div className="newSkill__uploadWrapper">
          <label htmlFor="contained-button-file">
            <Input
              accept="image/*"
              id="contained-button-file"
              multiple
              type="file"
              onChange={onChangeLogo}
            />
            <Button variant="contained" component="span">
              Upload logo
            </Button>
          </label>
        </div>
      </div>

      <div className="newSkill__btns">
        <Button variant="outlined" onClick={hideModal}>
          Cancel
        </Button>
        <Button
          variant="contained"
          onClick={onSave}
          disabled={!newSkill.title || !newSkill.type || !newSkill.subType}>
          Submit
        </Button>
      </div>
    </div>
  )
}

export default NewSkillForm
