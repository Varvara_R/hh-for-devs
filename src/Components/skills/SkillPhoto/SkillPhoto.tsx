import React, { useState, useEffect, FC } from "react";
import "./SkillPhoto.scss";
// @ts-ignore
import default_avatar from "../../../img/default_avatar.jpg";
import { makeCancelable } from "../../../helperFns";
import { environment } from "../../../environment/environment";

type Props = {
  skillTitle: string;
  width?: number;
  height?: number;
};

const SkillPhoto: FC<Props> = ({ skillTitle, width, height }) => {
  const [image, setImage] = useState(default_avatar);

  useEffect(() => {
    const myHeaders = new Headers();
    const requestOptions = {
      method: "GET",
      headers: myHeaders,
      responseType: "arraybuffer",
    };
    const cancelabelPromise = makeCancelable(
      fetch(
        `${environment.apiUrl}/skills/${skillTitle}/image`,
        requestOptions
      ).then((res) => {
        if (!res.ok) {
          res.text().then((text) => {
            const data = text && JSON.parse(text);
            const error = (data && data.message) || res.statusText;
            return Promise.reject(error);
          });
        }
        return res;
      })
    );
    cancelabelPromise.promise
      .then((res) => res.blob())
      .then((blob) => {
        const objectUrl = URL.createObjectURL(blob);
        setImage(objectUrl);
      })
      .catch((reason) => {});

    return () => cancelabelPromise.cancel();
  }, [skillTitle]);

  return (
    <div
      className="skill__image"
      style={width && height ? { width: width, height: height } : {}}
    >
      <img src={image} alt={skillTitle} />
    </div>
  );
};

export default SkillPhoto;
