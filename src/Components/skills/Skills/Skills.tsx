import React, { useState, useEffect, useCallback } from 'react'
import './Skills.scss'
import { AppStateType } from '../../../redux/reducers/rootReducer'
import { useSelector, useDispatch } from 'react-redux'
import { skillsAction } from '../../../redux/actions/skillActions'
import NewSkillForm from '../NewSkillForm'
import Skill from '../Skill'
import { ISkill } from '../../../interfaces/skill.interface'
import Modal from '@mui/material/Modal'
import Button from '@mui/material/Button'
import Autocomplete from '@mui/material/Autocomplete'
import TextField from '@mui/material/TextField'
import { Add } from '@mui/icons-material'
import CircularProgress from '@mui/material/CircularProgress'

const Skills = () => {
  const isLoggedIn = useSelector((state: AppStateType) => state.user.isLoggedIn)
  const skills = useSelector((state: AppStateType) => state.skills.skills)
  const skillsCount = useSelector((state: AppStateType) => state.skills.count)

  console.log(skills)
  console.log(skillsCount)
  const dispatch = useDispatch()
  const getAllSkills = useCallback(() => {
    dispatch(skillsAction.getAllSkills())
  }, [])
  const addNewSkill = useCallback((newSkill: ISkill) => {
    dispatch(skillsAction.create(newSkill))
  }, [])
  const [isNewSkillModal, setShowNewSkillModal] = useState<boolean>(false)
  const showNewSkillModal = () => setShowNewSkillModal(true)
  const hideNewSkillModal = () => setShowNewSkillModal(false)

  useEffect(() => {
    getAllSkills()
  }, [])

  if (!skills) {
    return (
      <div className="loading">
        <CircularProgress size={70} />
      </div>
    )
  }

  return (
    <>
      <div className="skills">
        {skills && (
          <div className="skills__searchBar">
            <Autocomplete
              sx={
                isLoggedIn
                  ? { width: '94%', marginRight: '10px' }
                  : { width: '100%' }
              }
              options={skills}
              getOptionLabel={(option) => option.title}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="Skill search"
                  placeholder="Search for skill..."
                />
              )}
            />

            {isLoggedIn && (
              <Button onClick={showNewSkillModal}>
                <div className="material-icons">
                  <Add />
                </div>
              </Button>
            )}
          </div>
        )}

        <div className="skills__container">
          {skills ? (
            skills.map((skill) => {
              return <Skill key={skill.title} skillName={skill.title} />
            })
          ) : (
            <p> No skills were found</p>
          )}
        </div>
      </div>

      {isLoggedIn && (
        <Modal
          open={isNewSkillModal}
          onClose={hideNewSkillModal}
          aria-labelledby="modal-new-skill"
          aria-describedby="modal">
          <NewSkillForm
            addNewSkill={addNewSkill}
            hideModal={hideNewSkillModal}
          />
        </Modal>
      )}
    </>
  )
}

export default Skills
