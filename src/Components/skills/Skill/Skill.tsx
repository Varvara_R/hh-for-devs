import React, { FC, useState, useEffect } from "react";
import "./Skill.scss";
import SkillPhoto from "../SkillPhoto";

type Props = {
  skillName: string;
};
const Skill: FC<Props> = ({ skillName }) => {
  return (
    <div className="skill">
      <SkillPhoto skillTitle={skillName} />
      <h4>{skillName}</h4>
    </div>
  );
};

export default Skill;
