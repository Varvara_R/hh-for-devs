export interface ISkill {
    _id?: string,
    title: string,
    type: string,
    subType: string,
    lastVersion?: string,
    officialSite?: string,
    git?: string,
    wiki?: string,
    description?: string,
    relatedLinks?: Array<RelatedLink>,
    logo?: string
}

export interface RelatedLink {
    title: string,
    url: string
}