export interface IVacancy {
    id: string,
    name: string,
    published: string,
    salary: SalaryData | null,
    area: AreaData,
    employer: EmployerData,
    schedule: ScheduleData,
    snippet: SnippetData,
    type: TypeData
}

interface SalaryData {
    from: number | null,
    to: number | null,
    currency: string,
    gross: boolean
}
interface AreaData {
    id: string,
    name: string,
    url: string
}
interface EmployerData {
    id: string,
    name: string,
    url: string,
    trusted: boolean,
    vacancies_url: string
}

interface ScheduleData {
    id: 'flexible' | 'remote' |'fullDay'
    name: string
}

interface SnippetData {
    requirement: string,
    responsibility: string
}

// @TODO check add instead string names: 'open'|'closed'
interface TypeData {
    id: 'open',
    name: string
}