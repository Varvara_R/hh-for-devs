export interface IUser {
  email: string;
  password: string;
  name: string;
  roadmaps: [];
  skills: [];
  avatar: string;
  admin?: boolean;
}
