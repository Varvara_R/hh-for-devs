import { CSSProperties } from 'react'
import { Elements } from 'react-flow-renderer'

export interface IRoadmap {
  _id?: string
  title: string
  data: DataInterface
  created: string | Date
  description?: string
  speciality: string
}

interface DataInterface {
  // elements: Array<NodeElement | EdgeElement>
  elements: Elements
  currentId: string
  skills: Array<string>
}

interface PositionInterface {
  x: number
  y: number
}
interface DataElement {
  label: string
}
export interface NodeElement {
  id: string
  position: PositionInterface
  data?: DataElement
  type: 'customDefaultNode' | 'customInputNode' | 'customOutputNode'
  sourcePosition?: 'top' | 'top' | 'left' | 'right'
  targetPosition?: 'top' | 'top' | 'left' | 'right'
  style?: CSSProperties
  className?: string
  isHidden?: boolean
  draggable?: boolean
  connectable?: boolean
  selectable?: boolean
  dragHandle?: string
}

export interface EdgeElement {
  id: string
  source: string
  target: string
  type: 'default' | 'straight' | 'step' | 'smoothstep'
  sourceHandle?: string
  targetHandle?: string
  animated?: boolean
  label?: string
  arrowHeadType: 'arrow' | 'arrowclosed'
  style?: CSSProperties
  className?: string
}
