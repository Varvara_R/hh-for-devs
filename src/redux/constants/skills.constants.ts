export enum skillsConstants {
    GET_ALL_SKILLS = "GET_ALL_SKILLS",
    CREATE_SKILL = "CREATE_SKILL",
}