export enum userConstants  {
    SET_CURRENT_USER = "SET_CURRENT_USER",
    USER_LOGIN = "USER_LOGIN",
    USER_LOGOUT = "USER_LOGOUT"
}