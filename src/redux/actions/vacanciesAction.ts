import { VacanciesType } from '../types'
import { FETCH_VACANCIES } from '../constants/vacancies.constants'

export const vacanciesAction = {
  getVacancies,
}
function getVacancies() {
  return async (dispatch) => {
    try {
      const response = await fetch(
        'https://api.hh.ru/vacancies?text=junior javascript&area=2&per_page=100'
      )
      const json = await response.json()

      dispatch({ type: FETCH_VACANCIES, payload: json })
    } catch (err) {
      console.log(err)
    }
  }
}
