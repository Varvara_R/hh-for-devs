import { userConstants } from '../constants/user.constants';
import { environment } from "../../environment/environment";
import axios from "axios";

const setUser = (user) => {
    return ({
        type: userConstants.SET_CURRENT_USER,
        user: user
    })
}

const login = (email: string, password: string) => {
    return async (dispatch) => {
        try {
            const response = await axios.post(`${environment.apiUrl}/auth/login`, {
                email,
                password
            })
            dispatch({ type: userConstants.USER_LOGIN, user: response.data.user});
            localStorage.setItem('token', response.data.token)
            console.log(response.data)
        } catch(err){
            console.log(err)
        }
    }
  
}

const registration = (email, password, name) => {
    return async (dispatch) => {
        try {
            const response = await axios.post(`${environment.apiUrl}/auth/registration`, {
                email,
                password,
                name,
            })
            dispatch({ type: userConstants.USER_LOGIN, user: response.data})
        } catch(err) {
            console.log(err)
        }
    }
}
const logout = () => {
    return ({
        type: userConstants.USER_LOGOUT
    })
}
export const userAction = {
    setUser,
    login,
    registration,
    logout,
}