import { roadmapConstants } from '../constants/roadmap.constants'
import { environment } from '../../environment/environment'
import axios from 'axios'
import { IRoadmap } from '../../interfaces/roadmap.interface'

export const roadmapActions = {
  getAllRoadmaps,
  update,
  create,
  getRoadmapById,
}

function getAllRoadmaps() {
  return async (dispatch) => {
    try {
      const response = await axios.get(`${environment.apiUrl}/roadmaps`)
      dispatch({
        type: roadmapConstants.GET_ALL_ROADMAPS,
        roadmaps: response.data.roadmaps,
      })
    } catch (err) {
      console.log(err)
    }
  }
}

function update(roadmap: IRoadmap) {
  return async (dispatch) => {
    try {
      const { data: data } = roadmap
      const response = await axios.put(
        `${environment.apiUrl}/roadmaps/${roadmap._id}`,
        { data: data }
      )
      dispatch({
        type: roadmapConstants.UPDATE_ROADMAP,
        roadmap: response.data.roadmap,
      })
    } catch (err) {
      console.log(err)
    }
  }
}

function create(newRoadmap: IRoadmap) {
  return async (dispatch) => {
    try {
      const response = await axios.post(
        `${environment.apiUrl}/roadmaps`,
        newRoadmap
      )
      dispatch({
        type: roadmapConstants.CREATE_ROADMAP,
        roadmaps: response.data.roadmaps,
        count: response.data.count,
      })
    } catch (err) {
      console.log(err)
    }
  }
}

function getRoadmapById(id: string) {
  return async (dispatch) => {
    try {
      const response = await axios.get(`${environment.apiUrl}/roadmaps/${id}`)
      dispatch({
        type: roadmapConstants.GET_ROADMAP_BY_ID,
        roadmap: response.data,
      })
    } catch (err) {
      console.log(err)
    }
  }
}
