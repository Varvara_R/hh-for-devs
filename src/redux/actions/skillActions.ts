import { skillsConstants } from "../constants/skills.constants";
import { environment } from "../../environment/environment";
import axios from "axios";
import { ISkill } from "../../interfaces/skill.interface";

export const skillsAction = {
    getAllSkills,
    create,
}

function getAllSkills() {
    return async(dispatch) => {
        try {
            const response = await axios.get(`${environment.apiUrl}/skills`)
            dispatch({type: skillsConstants.GET_ALL_SKILLS, skills: response.data.skills, count: response.data.count})
        }
        catch(err){
            console.log(err)
        }
    }
};

function urlToFile(url: any, fileName: any, mimeType: any){
    return (fetch(url)
        .then(function(res){ return res.arrayBuffer();})
        .then(function(buf){ return new File([buf], fileName, {type: mimeType});})
    )
}

function create(newSkill: ISkill){
    return async(dispatch) => {
        const { logo, ...newSkillData } = newSkill;
        const file = await urlToFile(logo, 'logo.png', 'image/png');
        const data = new FormData();
        data.append('file', file, file.name);
        data.append('data', JSON.stringify(newSkillData));
        const response = await axios.post(`${environment.apiUrl}/skills`, data);
        dispatch({type: skillsConstants.CREATE_SKILL, skills: response.data.skills, count: response.data.count})
    }
}