import { combineReducers } from "redux";
import { vacanciesReducer } from "./vacancyReducer";
import { userReducer } from "./userReducer";
import { skillsReducer } from "./skillsReducer";
import { roadmapsReducer } from "./roadmapsReducer";

export const rootReducer = combineReducers({
  user: userReducer,
  vacancies: vacanciesReducer,
  skills: skillsReducer,
  roadmaps: roadmapsReducer,
});

export type RootReducerType = typeof rootReducer;
export type AppStateType = ReturnType<RootReducerType>;