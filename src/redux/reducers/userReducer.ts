import { IUser } from "../../interfaces/user.interface";
import { userConstants } from "../constants/user.constants";

type InitialStateType = {
    currentUser: IUser | null
    isLoggedIn: boolean,
    isLogging: boolean
}
const initialState: InitialStateType = {
    currentUser: null,
    isLoggedIn: false,
    isLogging: false,
};
export function userReducer(state = initialState, action: any): InitialStateType {
  switch (action.type) {
    case userConstants.SET_CURRENT_USER:
      return { 
          ...state, 
          currentUser: action.user,
          isLoggedIn: true
        };
    case userConstants.USER_LOGIN:
        return {
            ...state,
            isLogging: false,
            isLoggedIn: true,
            currentUser: action.user
        }
    case userConstants.USER_LOGOUT:
        localStorage.removeItem('token')
        return {
            ...state,
            isLoggedIn: false,
            currentUser: null,
            isLogging: false,
        }
    default:
      return state;
  }
};
