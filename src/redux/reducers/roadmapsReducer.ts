import { roadmapConstants } from "../constants/roadmap.constants";
import { IRoadmap } from "../../interfaces/roadmap.interface";

type InitialStateType = {
    roadmaps: Array<IRoadmap> | null,
    roadmap?: IRoadmap,
    count?: Number,
}

const initialState: InitialStateType= {
    roadmaps: null
}

export function roadmapsReducer(state = initialState, action: any): InitialStateType {
    switch(action.type){
        case roadmapConstants.GET_ALL_ROADMAPS:
            return {
                ...state,
                roadmaps: action.roadmaps,
                count: action.count,
            }
        case roadmapConstants.UPDATE_ROADMAP:
            return {
                ...state,
                roadmap: action.roadmap
            }
        case roadmapConstants.CREATE_ROADMAP:
            return {
                ...state,
                roadmaps: action.roadmaps,
                count: action.count,
            }
        case roadmapConstants.GET_ROADMAP_BY_ID:
            return {
                ...state,
                roadmap: action.roadmap
            }
        default:
            return state;
    }
}