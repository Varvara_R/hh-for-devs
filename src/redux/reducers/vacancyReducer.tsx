import { IVacancy } from "../../interfaces/vacancy";
import { FETCH_VACANCIES } from "../constants/vacancies.constants";

type InitialStateType = {
    fetchVacancies: Array<IVacancy>
}
const initialState: InitialStateType = {
    fetchVacancies: [],
};
export function vacanciesReducer(state = initialState, action: any): InitialStateType {
  switch (action.type) {
    case FETCH_VACANCIES:
      return { ...state, fetchVacancies: action.payload.items };
    default:
      return state;
  }
};
