import { ISkill } from "../../interfaces/skill.interface";
import { skillsConstants } from "../constants/skills.constants";

type InitialStateType = {
    skills: Array<ISkill> | null,
    skill?: ISkill,
    count?: number
}

const initialState: InitialStateType = {
    skills: null,
}

export function skillsReducer(state = initialState, action: any): InitialStateType {
    switch(action.type){
        case skillsConstants.GET_ALL_SKILLS:
            return {
                ...state,
                skills: action.skills,
                count: action.count
            }
        case skillsConstants.CREATE_SKILL:
            return {
                ...state,
                skills: action.skills,
                count: action.count
            }
        default:
            return state;
    }
};