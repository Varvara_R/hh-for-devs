import { FETCH_VACANCIES } from "./constants/vacancies.constants";
import { IVacancy } from "../interfaces/vacancy";

type GetFetchVacancies = {
    type: typeof FETCH_VACANCIES,
    payload: Array<IVacancy>
}
export type VacanciesType = 
 | GetFetchVacancies